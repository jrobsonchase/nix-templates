{
  description = "Personal nix templates";

  inputs = {
    templates.url = "github:NixOS/templates";
  };

  outputs = inputs@{ self, ... }: {
    templates = inputs.templates.templates // {
      cargo2nix = {
        path = ./cargo2nix;
        description = "Rust template, using cargo2nix";
      };
      rust = {
        path = ./rust;
        description = "Rust template, using naersk";
      };
    };
  };
}
