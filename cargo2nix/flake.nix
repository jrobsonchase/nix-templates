{
  description = "A Rust project using cargo2nix";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    cargo2nix = {
      url = "github:cargo2nix/cargo2nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, cargo2nix, flake-utils, ... }:
    let
      cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
      name = cargoToml.package.name;
    in
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = cargo2nix.overlays.${system};
        };

        rustPkgs = pkgs.rustBuilder.makePackageSet' {
          rustChannel = "1.56.1";
          packageFun = import ./Cargo.nix;
        };
      in
      rec {
        packages = builtins.mapAttrs (k: v: (v { }).bin) rustPkgs.workspace;

        defaultPackage = builtins.getAttr name packages;

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            cargo2nix.packages.${system}.cargo2nix
            pkgs.rust-analyzer
          ]
          ++ defaultPackage.buildInputs
          ++ defaultPackage.nativeBuildInputs;
          RUST_SRC_PATH = "${rustPkgs.rustChannel}/lib/rustlib";
        };
      }
    );
}
